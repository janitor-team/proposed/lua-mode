lua-mode (20210802-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 20210802

 -- Hilko Bengen <bengen@debian.org>  Mon, 27 Dec 2021 02:17:55 +0100

lua-mode (20201010-1) unstable; urgency=medium

  * Team upload.

  [ David Krauser ]
  * Update maintainer email address

  [ Dhavan Vaidya ]
  * d/control: Change Vcs-{Browser,Git} URL to salsa.debian.org

  [ Hilko Bengen ]
  * Add watch file
  * New upstream version 20201010
  * Drop patches
  * Drop transitional package

 -- Hilko Bengen <bengen@debian.org>  Sun, 11 Oct 2020 11:42:32 +0200

lua-mode (20151025-5) unstable; urgency=medium

  * Team upload.
  * Regenerate source package with quilt patches

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 15:48:32 -0300

lua-mode (20151025-4) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 10:32:34 -0300

lua-mode (20151025-3) unstable; urgency=medium

  * Change tests to work with buttercup 1.9, thanks to buttercup author
    Jorgen Schäfer for advice (Closes: #877398)

 -- Hilko Bengen <bengen@debian.org>  Thu, 14 Dec 2017 11:23:34 +0100

lua-mode (20151025-2) unstable; urgency=medium

  * Give package to pkg-emacsen-addons
  * Modernize package: elpa, copyright, Standards-Version, Debhelper
    compat level

 -- Hilko Bengen <bengen@debian.org>  Wed, 02 Aug 2017 23:46:10 +0200

lua-mode (20151025-1) unstable; urgency=medium

  * New upstream version
  * Modernize package
    - Debhelper 9
    - Bump Standards-Version
    - Add Vcs-* headers
    - Mention lua5.3, luajit
    - Drop patch

 -- Hilko Bengen <bengen@debian.org>  Sun, 01 Nov 2015 18:23:47 +0100

lua-mode (20140514-2) unstable; urgency=medium

  * Dropped emacs22 support (Closes: #775389)

 -- Hilko Bengen <bengen@debian.org>  Fri, 16 Jan 2015 10:41:29 +0100

lua-mode (20140514-1) unstable; urgency=medium

  * New upstream version
  * Updated watch file from sepwatch
  * Bumped Standards-Version

 -- Hilko Bengen <bengen@debian.org>  Thu, 17 Jul 2014 11:59:17 +0200

lua-mode (20130419-4) unstable; urgency=medium

  * Changed auto-mode-alist entry so startup file and lua-mode.el match
    (Closes: #680697)
  * Added patch to make lua-indent-level a safe local-variable (Closes:
    #717431)

 -- Hilko Bengen <bengen@debian.org>  Wed, 04 Jun 2014 20:23:34 +0200

lua-mode (20130419-3) unstable; urgency=low

  * Don't build for emacs21, xemacs21 (Closes: #718826, #718827)

 -- Hilko Bengen <bengen@debian.org>  Tue, 06 Aug 2013 16:41:04 +0200

lua-mode (20130419-2) unstable; urgency=low

  * Fixed install file (Closes: #718811)

 -- Hilko Bengen <bengen@debian.org>  Mon, 05 Aug 2013 22:26:40 +0200

lua-mode (20130419-1) unstable; urgency=low

  * Adopted package (Closes: #628938)
  * New upstream version
    - No longer uses last-command-char which has been deprecated since
      Emacs 19.34 (Closes: #705634)
    - Correctly indents/aligns multi-line conditions (Closes: #546467)
    - Correctly highlights multi-line comments (Closes: #602301)
  * Updated watch file
  * Various cleanups
    - Bumped Standards-Version, updated Homepage, stripped
      build-dependencies
    - Simplified debian/rules

 -- Hilko Bengen <bengen@debian.org>  Thu, 01 Aug 2013 18:26:00 +0200

lua-mode (20110121-1) unstable; urgency=low

  * New upstream version, which includes patch multiline-strings.diff.
  * Upstream has apparently moved to github.

 -- Jens Peter Secher <jps@debian.org>  Tue, 22 Mar 2011 07:06:01 +0100

lua-mode (20100617-1) experimental; urgency=low

  * New upstream version.
    (Closes: #601678)
  * Removed a keybinding patch and some open bugs because they seem to be
    obsoleted by the new uptream version, but it is somewhat hard to tell
    in the absence of changelogs and release notes, but here we go:
    (Closes: #546465,#546463)
  * Added Lua 5.1 to the enhancement list.
    (Closes: #574519)

 -- Jens Peter Secher <jps@debian.org>  Sat, 30 Oct 2010 10:30:35 +0200

lua-mode (20071122-4) unstable; urgency=low

  * Converted to dpkg-source format 3.0 (quilt).
  * Use mercurial-buildpackage for repository maintenance, so old
    repository does not exist any more.
  * Include ${misc:Depends} so that debhelper can inject items into the
    dependency list.
  * Bumped Standards-Version to 3.9.1, no change.

 -- Jens Peter Secher <jps@debian.org>  Sat, 30 Oct 2010 09:49:01 +0200

lua-mode (20071122-3) unstable; urgency=low
  
  * Leave elisp source file links next to byte-compiled files so that info
    system can locate the source code.
    (Closes: #543492)
  * Fontify multiline strings.
    (Closes: #466130)  
  * Use quilt instead of dpatch for patches.
  * Use debhelper 7.
  * Upgraded to Standards-Version 3.8.3.
    + Added a Homepage control field.
    + Added a Vcs-Hg control filed to indicate the location of the public
      repository.
    + Describe how to use quilt in Debian.source (thanks to Russ Allbery).
  * Do not load site/init files during byte compilation.

 -- Jens Peter Secher <jps@debian.org>  Sun, 30 Aug 2009 17:20:20 +0200

lua-mode (20071122-2) unstable; urgency=low

  * Changed unusual keyboard binding to standard ones, thanks to Trent
    W. Buck.
    (Closes: bug#468298)
  * Bumped Standards-Version to 3.7.3.

 -- Jens Peter Secher <jps@debian.org>  Sun, 02 Mar 2008 10:32:36 +0100

lua-mode (20071122-1) unstable; urgency=low

  * New upstream release.
  * Bumped debhelper compatibility version to 5.

 -- Jens Peter Secher <jps@debian.org>  Sun, 02 Dec 2007 13:24:50 +0100

lua-mode (20070703-2) unstable; urgency=low

  * Use autoload instead of load, thanks to Trent W. Buck.
    (Closes: bug#446234)

 -- Jens Peter Secher <jps@debian.org>  Mon, 15 Oct 2007 22:05:28 +0200

lua-mode (20070703-1) unstable; urgency=low

  * New upstream release.
  * Simplify install, removal, and load-path by using symlinks instead of
    copying source files.

 -- Jens Peter Secher <jps@debian.org>  Sun, 22 Jul 2007 23:17:59 +0200

lua-mode (20061208-1) unstable; urgency=low

  * New upstream release. 
    (Closes: bug#402133)
    - which fixes typo in docstring.
    (Closes: bug#394206) 
    - which obsoletes my patch for multiline comments, so dpatch is no
      longer needed.
  * Include a watch file.

 -- Jens Peter Secher <jps@debian.org>  Sun, 10 Dec 2006 13:30:39 +0100

lua-mode (20060625-1) unstable; urgency=low

  * New upstream release.
    (Closes: bug#390588)
  * Bumped Standards-Version to 3.7.2.

 -- Jens Peter Secher <jps@debian.org>  Wed,  4 Oct 2006 22:56:21 +0200

lua-mode (1.74-2) unstable; urgency=low

  * Show commented out multiline comments as non-comments... 'mkay!
    (Closes: bug#350277)

 -- Jens Peter Secher <jps@debian.org>  Thu,  9 Feb 2006 21:55:57 +0100

lua-mode (1.74-1) unstable; urgency=low

  * New upstream version.
    + Fixes broken indentation.
    (Closes: bug#345316)
  * Use the right path to the lua executable.
  * Automatically turn on lua-mode for lua files.
  * Make sure that Emacs Help can find the source code by including the
    location in load-path.

 -- Jens Peter Secher <jps@debian.org>  Mon,  2 Jan 2006 22:25:57 +0100

lua-mode (1.26-1) unstable; urgency=low

  * Initial release.
    (Closes: bug#266160)

 -- Jens Peter Secher <jps@debian.org>  Tue,  2 Nov 2004 21:26:59 +0100

